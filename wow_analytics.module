<?php
/**
 * @file
 * Drupal Module: WOW Analytics.
 *
 * Adds the required Javascript to all your Drupal pages to allow tracking by
 * the WOW Analytics statistics package.
 */

/**
 * Implements hook_help().
 */
function wow_analytics_help($path, $arg) {
  switch ($path) {
    case 'admin/config/system/wow_analytics':
      return t('<a href="@wow_url">WOW Analytics</a> is a website traffic and marketing service.', array('@wow_url' => 'http://www.wowanalytics.co.uk'));
  }
}

/**
 * Implements hook_permission().
 */
function wow_analytics_permission() {
  return array(
    'administer wow analytics' => array(
      'title' => t('Administer WOW Analytics'),
      'description' => t('Configure WOW Analytics.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function wow_analytics_menu() {
  $items['admin/config/system/wow_analytics'] = array(
    'title' => 'WOW Analytics',
    'description' => 'Configure tracking for WOW analytics.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wow_analytics_admin_settings_form'),
    'access arguments' => array('administer wow analytics'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'wow_analytics.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_page_alter().
 */
function wow_analytics_page_alter(&$page) {
  $id = variable_get('wow_analytics_client_id', '');
  $scope = variable_get('wow_analytics_js_scope', 'header');
  $js = wow_analytics_script($id);

  drupal_add_js($js, array('scope' => $scope, 'type' => 'inline'));
}

/**
 * Generate the js script to embed.
 *
 * @param string $id
 *   The WOW client id.
 *
 * @return string
 *   JS that will be embedded on the page.
 */
function wow_analytics_script($id) {
  $js = <<<JS
    var _wow = _wow || [];
    (function () {
    try{
                    _wow.push(['setClientId', '{$id}']);
                    _wow.push(['enableDownloadTracking']);
                    _wow.push(['trackPageView']);
                    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
                    g.type = 'text/javascript'; g.defer = true; g.async = true;
                    g.src = '//t.wowanalytics.co.uk/Scripts/tracker.js';
                    s.parentNode.insertBefore(g, s);
    }catch(err){}})();
JS;
  return $js;
}
